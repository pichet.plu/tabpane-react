import React, { Component } from 'react'
import className from 'classnames'
import PropTypes from 'prop-types';

export default class TabsComponent extends Component {
    static defaultProps = {
        activeIndex: 0
    }
    static PropTypes = {
        activeTab:PropTypes.number.isRequired,
        children:PropTypes.arrayOf(PropTypes.element)
    }
    tabLinkCalsses = (index) => {
        return className('nav-link', { active: index === this.state.activeTab })
    }
    state = {
        activeTab: this.props.activeTab
    }
    setActiveTab = (index) => {
        this.setState({ activeTab: index })
    }

    render() {
        return (
            <div>
                <ul className='nav nav-tabs'>
                    {
                        this.props.children.map(
                            (pane, index) => <li className='nav-item '>
                                <a href="#" className={this.tabLinkCalsses(index)}
                                    onClick={() => this.setActiveTab(index)}>
                                    {pane.props.title}
                                </a>
                            </li>
                        )
                    }
                </ul>
                <div className='tab-content'>
                    {this.props.children[this.state.activeTab]}
                </div>
            </div>

        )
    }
}